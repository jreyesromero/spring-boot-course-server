DROP TABLE IF EXISTS COURSES;

CREATE TABLE COURSES (
  ID            INT(11)         NOT NULL AUTO_INCREMENT,
  TITLE   		VARCHAR(250)  	NOT NULL,
  LEVEL			VARCHAR(80)  	NOT NULL,
  HOURS		   	INT(3)  		NULL,
  TEACHER		VARCHAR(250)  	NOT NULL,
  STATE		 	INT(1)  		NOT NULL,
  PRIMARY KEY (ID),
  UNIQUE KEY (TITLE)
);

INSERT INTO COURSES (ID, TITLE, LEVEL, HOURS, TEACHER, STATE) VALUES(1, 'Aumenta el rendimiento de tus tests de integración con BBDD', 'Intermedio', 25, 'Alberto Barranco Ramó', 1);
INSERT INTO COURSES (ID, TITLE, LEVEL, HOURS, TEACHER, STATE) VALUES(2, 'Aplicando TDD', 'Basico', 10, 'Alejandro Acebes Cabrera', 1);
INSERT INTO COURSES (ID, TITLE, LEVEL, HOURS, TEACHER, STATE) VALUES(3, 'Ejecutando test de SoapUI Open Source en JUnit en un proyecto Maven', 'Basico', 20, 'Alberto Moratilla Ocaña', 1);
INSERT INTO COURSES (ID, TITLE, LEVEL, HOURS, TEACHER, STATE) VALUES(4, 'Copia y pega de StackOverflow', 'Intermedio', 15, 'Paco Luque', 0);
INSERT INTO COURSES (ID, TITLE, LEVEL, HOURS, TEACHER, STATE) VALUES(5, 'Primeros pasos con Docker', 'Intermedio', 25, 'Paco Luque', 1);
INSERT INTO COURSES (ID, TITLE, LEVEL, HOURS, TEACHER, STATE) VALUES(6, 'Git nivel experto', 'Avanzado', 25, 'Jose Luis Gomez', 1);
INSERT INTO COURSES (ID, TITLE, LEVEL, HOURS, TEACHER, STATE) VALUES(7, 'Streams en Java 8', 'Intermedio', 25, 'Jose Luis Gomez', 1);
INSERT INTO COURSES (ID, TITLE, LEVEL, HOURS, TEACHER, STATE) VALUES(8, 'Copy paste de StackOverflow', 'Avanzado', 25, 'Jose Luis Gomez', 1);