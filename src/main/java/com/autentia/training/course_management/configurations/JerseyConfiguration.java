package com.autentia.training.course_management.configurations;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.autentia.training.course_management.web.resources.CourseResource;
import com.autentia.training.course_management.web.filter.CORSFilter;

@Configuration 
@ApplicationPath("/api")
public class JerseyConfiguration extends ResourceConfig {

	public JerseyConfiguration() {
		register(CourseResource.class);
		register(CORSFilter.class);
	}
}
