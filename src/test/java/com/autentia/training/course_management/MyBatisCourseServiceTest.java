package com.autentia.training.course_management;

import com.autentia.training.course_management.model.services.mybatis.MyBatisCourseService;
import com.autentia.training.course_management.model.mappers.CourseMapper;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;

import com.autentia.training.course_management.model.entities.Course;
import com.autentia.training.course_management.web.resources.CourseResource;
import com.autentia.training.course_management.model.services.CourseService;
import javax.ws.rs.core.Response;

import javax.persistence.EntityNotFoundException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyBatisCourseServiceTest {
	
	private MyBatisCourseService myBatisCourseService;
	private Course courseMock;
	private Long id;

	
	@Before
	public void setup() {
		CourseMapper mockCourseMapper = mock(CourseMapper.class);
		myBatisCourseService = new MyBatisCourseService(mockCourseMapper);
		
		courseMock = mock(Course.class);
		id = Long.valueOf(100);
		when(courseMock.getId()).thenReturn(id);
		when(courseMock.getTitle()).thenReturn("Primeros pasos con Java 8");
		when(courseMock.getLevel()).thenReturn("Basico");
		when(courseMock.getTeacher()).thenReturn("Pedro Antonio Lopez");
		when(courseMock.getState()).thenReturn(true);
	}
	
	@Test(expected = EntityNotFoundException.class)
	public void checkMyBatisServiceUpdateByIdThrowsException() {
		myBatisCourseService.update(id,courseMock);
	}
	
	@Test(expected = EntityNotFoundException.class)
	public void checkMyBatisServiceDeleteByIdThrowsException() {
		myBatisCourseService.delete(id);
	}
	
	@Test(expected = EntityNotFoundException.class)
	public void checkMyBatisServiceFindByIdThrowsException() {
		myBatisCourseService.findOne(id);
	}
}

