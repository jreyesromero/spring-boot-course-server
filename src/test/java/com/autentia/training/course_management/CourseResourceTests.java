package com.autentia.training.course_management;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;

import com.autentia.training.course_management.model.entities.Course;
import com.autentia.training.course_management.web.resources.CourseResource;
import com.autentia.training.course_management.model.services.CourseService;
import javax.ws.rs.core.Response;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseResourceTests {
	
	CourseResource courseResource;
	
	@Before
	public void setup() {
		CourseService mockCourseService = mock(CourseService.class);
		courseResource = new CourseResource(mockCourseService);

	}

	@Test
	public void checkFieldsFromPostHttpRequest() {
		
		Course courseMock = mock(Course.class);
		when(courseMock.getTitle()).thenReturn("Primeros pasos con Java 8");
		when(courseMock.getLevel()).thenReturn("Basico");
		when(courseMock.getTeacher()).thenReturn("Pedro Antonio Lopez");
		when(courseMock.getState()).thenReturn(null);
		
		// test without all mandatory fields in course object
		assertEquals(courseResource.create(courseMock).getStatus(), 500);
		assertNull(courseResource.create(courseMock).getHeaderString("Access-Control-Allow-Origin"));

		// test WITH all mandatory fields in course object
		when(courseMock.getState()).thenReturn(true);
		assertEquals(courseResource.create(courseMock).getStatus(), 200);
		
	}
	
	@Test
	public void testDeleteHttpRequest() {
		Long id = Long.valueOf(1);
		
		Course courseMock = mock(Course.class);
		assertEquals(courseResource.delete(id).getStatus(), 200);
	}
	
	@Test
	public void tesGetByIdHttpRequest() {
		Long id = Long.valueOf(1);
		
		Course courseMock = mock(Course.class);
		assertEquals(courseResource.get(id).getStatus(), 200);
	}
	
	@Test 
	public void testUpdateCourseById() {
		Course courseMock = mock(Course.class);
		when(courseMock.getTitle()).thenReturn("Primeros pasos con Java 8");
		when(courseMock.getLevel()).thenReturn("Basico");
		when(courseMock.getTeacher()).thenReturn("Pedro Antonio Lopez");
		when(courseMock.getState()).thenReturn(true);
		assertEquals(courseResource.create(courseMock).getStatus(), 200);
		
	}
	

}

